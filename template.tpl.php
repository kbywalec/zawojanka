 <!DOCTYPE html>
<html lang="pl">
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<META HTTP-EQUIV="Content-Language" CONTENT="pl">
<META HTTP-EQUIV="Pragma" content="cache">
<META NAME="ROBOTS" CONTENT="all">
<meta name="Keywords" content="Ośrodek, wypoczynkowy, zawojanka, zawoja, wypoczynek, góry, noclegi, obozy taneczne, obozy sportowe, biesiady" />
<meta name="Description" content="Ośrodek Wypoczynkowy ZAWOJANKA – największa baza noclegowa w Zawoi. Tylko u nas – 170 miejsc w pokojach 2, 3, 4 i 5 – osobowych z pełnym węzłem sanitarnym oraz telewizorem." />
<META NAME="author" CONTENT="DOGMAT Sp. z o.o. http://dogmat.eu">
<META NAME="revisit-after" CONTENT="7 days">
<link href="/media/style/styles.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="/media/style/nivo-slider.css" type="text/css" media="screen" />

<title>Ośrodek Wypoczynkowy ZAWOJANKA, 34–223 ZAWOJA 981</title>
<!-- jshead -->
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.12&appId=397829073574445&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
<div id="outer-wrapper">
  <div id="top_bg">
    <div id="bottom_bg">
      <div id="wrap2">
        <div id="wrap3">
          <div id="wrap4">
            <div id="wrap5">
             
              
              <div id="header-wrapper">
              
             
                <div class="header section" id="header">
                
                  <div class="widget Header" id="Header1">
                  
                    <div id="header-inner">
                      <div class="titlewrapper">
                        <h1 class="title" id="blog_title"> <a rel="history" href="index">ZAWOJANKA</a></h1>
                      </div>
                      
                      
                      <div class="descriptionwrapper">
                        <h2>Ośrodek Wypoczynkowy</h2>
                      </div>
                      
                      
                        <div id="menu">
		                  <ul>
		                    <li class="menu_first"><a rel="history" href="onas">O Nas</a></li>
		                    <li><a rel="history" href="oferta">Oferta</a></li>
		                    <li><a rel="history" href="zawoja">Zawoja</a></li>
		                    <li><a rel="history" href="galeria">Galeria</a></li>
		                    <li><a rel="history" href="kontakt">Kontakt</a></li>
		                    <li><a rel="history" href="dojazd">Dojazd</a></li>
		                  </ul>
						</div>
				
				
                    </div>
                    
                  </div>
                  <div id="slider-wrapper">
                  <div id="slider" class="nivoSlider"><img src="/media/images/header.jpg" alt="Zawojanka"/><img src="/media/images/header2.jpg" alt="Zawojanka"/> <img src="/media/images/header3.jpg" alt="Zawojanka"/> </div>
                </div>
                
                
                
              </div>
              <script type="text/javascript" src="/media/lib/jquery.nivo.slider.pack.js"></script>
              <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>
            </div>
                </div>
                 
              
              </div>
              
             
              <div class="clearfix" id="content-wrapper">
                <div id="crosscol-wrapper" style="text-align:center">
                  <div class="crosscol section" id="crosscol"></div>
                </div>
                
                <div id="sidebar-wrapper">
                  <div id="sidebar_main">
                    <ul>
                      <li>
                      
                      
                        <div id="sidebar-main">
                          <div id="Label1">
                            <h2>OFERTY SPECJALNE</h2>
                            <div class="widget-content">
                              
                              <? echo \mea\articlesInFolder_menu::inst(['srcFolder'=>SITE_DIR.'content/oferty/','link'=>'oferty']); ?>
                              
                            </div>
                          </div>
                          <div class="fb-page" style="margin-top: 25px;" data-href="https://www.facebook.com/zawojanka/" data-tabs="timeline" data-width="200" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/zawojanka/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/zawojanka/">Zawojanka</a></blockquote></div>
                        </div>
                     
                        
                      </li>
                    </ul>
                  </div>
                </div>
                <div id="wrapper">
                  <div class="clearfix" id="main-wrapper">                    
                    <div id="MeaContent"><!--content--></div>
                  </div>
                </div>
              </div>
             
              <div id="footer-wrapper">
                <div class="credit"> <br/>
                  <strong>Ośrodek Wypoczynkowy ZAWOJANKA,</strong> 34–223 ZAWOJA 981, <strong>Tel./Fax</strong> 0(33) 877-51-22</div>
              <br>Realizacja: <a href="http://www.dogmat.eu">Dogmat sp. z o.o.</a> - <a href="http://www.pogodzinach.net">Noclegi w Polsce</a>
              </div>
             
            </div>
          </div>
        </div>
      </div>
    
</div>
<script language="javascript1.2" type="text/javascript" src=" http://js.pogodzinach.net/cookiewarning.js"></script>
</body>
</html>