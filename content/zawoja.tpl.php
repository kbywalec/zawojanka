
                <div class="main section" id="main">
                  <div class="widget Blog" id="Blog1">
                    <div class="blog-posts hfeed">
                      <div class="post hentry uncustomized-post-template">
                        <h2 class="post-title entry-title">Zawoja</h2>
                        <div class="entry">
                        
                          <P>Gmina Zawoja to dwie wsie - Zawoja i Skawica, rozsiane wokół drogi krajowej nr 957. Znana głównie dzięki królującej nad całą okolicą Królową Beskidów - Babią Górą (1725m.n.p.m.). Zawoja to jedna z najdłuższych (ok. 18 km) wsi w Polsce. Wydaje się, że można by w nieskończoność wymieniać: Zawoja Dolna, Zawoja Centrum, Zawoja Wełcza, Zawoja Mosorne, Zawoja Widły, Zawoja Markowa, Zawoja Policzne, Zawoja Górna, Zawoja Składy, Zawoja Czartoża… </P>
                          <P>Zawoja i jej okolice to przede wszystkim idealne miejsce do aktywnego wypoczynku. Mnogość szlaków turystycznych – pieszych, rowerowych, narciarskich i konnych oraz nagromadzenie różnego rodzaju atrakcji: </P>
                          <UL>
                            <LI>Skansen ''Markowe Rówienki'';
                            <LI>Skansen PTTK im. J. Żaka;
                            <LI>Orawski Park Etnograficzny w Zubrzycy;
                            <LI>Kościół pw. św. Klemensa (1888);
                            <LI>Klasztor Karmelitów Bosych;
                            <LI>kapliczki i kamienne figury rozsiane po całej wsi;
                            <LI>wodospad na Mosornym Potoku;
                            <LI>odnowione Schronisko na Markowych Szczawinach.
                          </UL>
                          <P>Ciekawostką jest, że nazwę Zawoja - z wołoskiego <I>zavoi</I> co znaczy ''las nad rzeką'', wprowadzono dopiero w sierpniu 1936 roku. Wcześniej Zawoja była częścią Skawicy i tak właśnie się nazywała. </P>
                          <P>Zawoja już w okresie międzywojennym stała się znaną miejscowością wypoczynkową. Dziś oferuje blisko 2,5 tysiąca miejsc noclegowych w pensjonatach i domach wypoczynkowych o różnym standardzie. W lecie zjeżdżają tu kuracjusze w poszukiwaniu ciszy, spokoju i kontaktu z naturą, natomiast zimą - amatorzy białego szaleństwa, których celem jest stacja narciarska Mosorny Groń.</P>
                          <P>Niewątpliwym plusem Zawoi jest komunikacja z Krakowem. Jeżdżące średnio co godzinę PKS-y i prywatne busy dowożą pasażerów w ciągu 1,5 - 2h. </P>
                          <P><U><B>Przykładowe piesze szlaki turystyczne </B></U> </P>
                          <P><STRONG><U>Szlak Żółty </U></STRONG><IMG SRC="media/images/zolty.png"><br>
                            <STRONG>Trasa:</STRONG> Zawoja Czatoża - Fickowe Rozstaje - Górny Płaj - Markowe Szczawiny</P>
                          <P><STRONG><U>Szlak
                            Zielony </U></STRONG><IMG SRC="media/images/zielony.png"><br>
                            <STRONG>Trasa:</STRONG> Zawoja Widły – Zawoja Składy - Zawoja Markowa - Pośredni Bór - Markowe Szczawiny</P>
                          <P><STRONG><U>Szlak Czarny </U></STRONG><IMG SRC="media/images/czarny.png"><br>
                            <B>Trasa:</B> Zawoja Składy – Zawoja Wałcza</P>
                          <P><STRONG><U>Szlak Żółty </U></STRONG><IMG SRC="media/images/zolty.png"><br>
                            <B>Trasa:</B> Zawoja Górna – Hala Śmietanowa </P>
                          <P><STRONG><U>Szlak Niebieski </U></STRONG><IMG SRC="media/images/niebieski.png"><br>
                            <STRONG>Trasa:</STRONG> Zawoja Czatoża – Zawoja Policzne – Przełęcz Krowiarki – Szkolnikowe Rozstaje – Markowe Szczawiny
                          <p><STRONG><U>Szlak Czarny </U></STRONG><IMG SRC="media/images/czarny.png"><br>
                            <STRONG>Trasa:</STRONG> Podryzowane - Ryzowane - Markowe Szczawiny</p>
                          <P><STRONG><U>Szlak Zielony </U></STRONG><IMG SRC="media/images/zielony.png"><br>
                            <STRONG>Trasa:</STRONG> Górny Płaj - Sokolica</P>
                          <P><STRONG><U>Szlak Żółty </U></STRONG><IMG SRC="media/images/zolty.png"><br>
                            <STRONG>Trasa:</STRONG> Zawoja Widły – Zawoja Składy – Zawoja Markowa - Markowe Szczawiny - Sucha Kotlinka – Babia Góra </P>
                          <P><STRONG><U>Szlak Czerwony </U></STRONG><IMG SRC="media/images/czerwony.png"><br>
                            <STRONG>Trasa:</STRONG> Polana Krowiarki - Sokolica - Kępa - Gówniak – Babia Góra - Przełęcz Brona - Markowe Szczawiny - Fickowe Rozstaje - Przełęcz Jałowiecka</P>
                          <P><STRONG><U>Szlak Zielony </U></STRONG><IMG SRC="media/images/zielony.png"><br>
                            <STRONG>Trasa:</STRONG> Przełęcz Jałowiecka - Mała Babia Góra - Przełęcz Brona – Babia Góra </P>
                          <P><STRONG><U>Szlak Zielony </U></STRONG><IMG SRC="media/images/zielony.png"><br>
                            <STRONG>Trasa:</STRONG> Polana Krowiarki - Hala Śmietanowa - Zubrzyca Górna</P>
                        </div>
                      </div>
                
                    </div>
                  </div>
                </div>
             