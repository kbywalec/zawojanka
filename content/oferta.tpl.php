<div class="main section" id="main">
    <div class="widget Blog" id="Blog1">
        <div class="blog-posts hfeed">
            <div class="post hentry uncustomized-post-template">
                <h2 class="post-title entry-title">Oferta</h2>

                <div class="entry">
                    <!--<img border="0" src="media/images/obiekt.jpg" align="left" hspace="10"/>-->
                    <h5>Ośrodek Wypoczynkowy <span class="accent-color">ZAWOJANKA</span> zaprasza!</h5>
                    <br/>


                    <p><strong>Ośrodek Wypoczynkowy "Zawojanka" to największa baza noclegowa w Zawoi, malowniczej miejscowości
                        położonej u stóp Babiej Góry.W jednym turnusie możemy przyjąć nawet 170 Gości!</strong></p>

                    <p>W "Zawojance" od blisko 15 lat gościmy turystów kochających góry, świeże powietrze, dobrą kuchnię
                        oraz prawdziwy wypoczynek :) Przygotowaliśmy dla Państwa niemal 50 pokoi z łazienkami, telewizją
                        i dostępem do wifi. Ponadto do Państwa dyspozycji pozostawiamy jadalnię, basen, boiska do
                        koszykówki i siatkówki, stół do ping-ponga, plac zabaw oraz miejsce na grill czy ognisko.</p>

                    <p>Dużym atutem naszego Ośrodka są trzy pełnowymiarowe sale lustrzane wykorzystywane przez naszych
                        Gości podczas zajęć tanecznych, sportowych,a także w trakcie szkoleń czy dyskotek.</p>

                    <p>Cały ośrodek łącznie z terenem rekreacyjnym, placem zabaw i parkingiem jest ogrodzony i
                        monitorowany.</p>

                    <p>Dla naszych Gości organizujemy również wycieczki fakultatywne, kuligi, programy artystyczne.</p>

                    <p>Dokładamy wszelkich starań, by nasi Goście, zarówno indywidualni, jak i biznesowi, czerpali
                        satysfakcję i zadowolenie z pobytu u nas. Nasz obiekt to przede wszystkim wysoki standard, jak i
                        przystępna cena.</p>

                    <p>Gwarantujemy wypoczynek w ciszy i spokoju, a dla szczęśliwców - niesamowitą panoramę całego
                        masywu Babiej Góry z okien pokoju! </p>
                    <br>


                    <h4>☺ ZAPEWNIAMY:</h4>
                    <ul>
                        <li>Poznanie osobliwości przyrodniczych Babiogórskiego Parku Narodowego – Światowego Rezerwatu
                            Biosfery
                        </li>
                        <li>Poznanie ciekawej kultury regionalnej</li>
                        <li>Wypoczynek wśród pięknych krajobrazów w ciszy i spokoju.</li>
                        <li>Program kulturalny: ogniska z pieczeniem kiełbasy, dyskoteki, konkursy z nagrodami</li>
                        <li> Możliwość rezerwacji hali sportowej (44m x 26 m) na zajęcia sportowe i treningi</li>
                        <li>Na miejscu w ośrodku 2 sale z parkietem o wymiarach 9 x 14 m – komfortowe warunki do zajęć
                            tanecznych (lustra), ćwiczeń, treningów – korzystanie w ramach kosztów pobytu, bez
                            dodatkowych opłat!
                        </li>
                    </ul>
                    <br/>
                    <h4>☺ OFERUJEMY w cenie:</h4>
                    <ul>
                        <li>Przygotowaliśmy dla Państwa pokoje 2,3 i 4-osobowe</li>
                        <li>W <u>KAŻDYM</u> pokoju <strong><u>ŁAZIENKA</u></strong> i <strong><u>TELEWIZOR</u></strong>
                        </li>
                        <li>3 posiłki dziennie (smaczna, świeża, regionalna kuchnia)</li>
                        <li>Sale dydaktyczne, bogate zaplecze do prowadzenia różnego rodzaju zajęć.</li>
                        <li> Kadra pedagogiczna ( bezpłatnie 1 nauczyciel/opiekun na 15 uczniów)</li>
                        <li>Imprezy rekreacyjno – sportowe na własnym kompleksie na terenie ośrodka</li>
                        <li><strong>GRATIS:</strong> Spokój, Cisza, Świeże powietrze oraz niesamowite widoki z okien
                            ośrodka na Królową Beskidów!
                        </li>
                    </ul>
                    <br/>
                    <h4>☺ DLACZEGO <font color="#CC0000">ZAWOJANKA</font>:</h4>
                    <ul>
                        <li>Ośrodek położony tuż przy Babiogórskim Parku Narodowym</li>
                        <li>Czyste powietrze oraz możliwość obcowania z naturą w pierwotnej postaci</li>
                        <li>Domowa atmosfera oraz świetna kuchnia!</li>
                        <li>Bezpieczeństwo: cały ośrodek (1,5ha) jest ogrodzony i monitorowany</li>
                        <li>Rekreacja: boiska do siatkówki i koszykówki, stół do ping-ponga, plac zabawa dla dzieci oraz
                            2 sale z parkietem (w budynku) o wymiarach 9m x 14m każda – idealne do zajęć tanecznych,
                            ćwiczeń, treningów
                        </li>
                        <li>Parking dla Gości</li>
                        <li>Miejsce na ognisko i grill</li>
                        <li>Konkurencyjne ceny</li>
                        <li><u><strong>Wieloletnie doświadczenie</strong> – co roku obsługujemy blisko <strong>5000
                                    gości!!!</strong></u></li>
                    </ul>
                    <br/>
                    <h4>☺ DLACZEGO ZAWOJA:</h4>
                    <ul>
                        <li>Bardzo dobra komunikacja z Krakowem – PKS i BUS – tylko 1,5h jazdy</li>
                        <li>Mnogość szlaków turystycznych: pieszych, rowerowych, konnych i narciarskich</li>
                        <li>Atrakcje turystyczne:
                            <ul>
                                <li>Babia Góra (1725m.n.p.m.);</li>
                                <li>Skansen „Markowe Rówienki”;</li>
                                <li>Skansen PTTK im. J. Żaka;</li>
                                <li>Orawski Park Etnograficzny w Zubrzycy;</li>
                                <li>Kościół pw. św. Klemensa (1888);</li>
                                <li>Klasztor Karmelitów Bosych;</li>
                                <li>kapliczki i kamienne figury rozsiane po całej wsi;</li>
                                <li>wodospad na Mosornym Potoku;</li>
                                <li>odnowione Schronisko na Markowych Szczawinach</li>
                            </ul>
                        </li>
                    </ul>
                    <br/>
                    <h4>☺ PROPONUJEMY TAKŻE:</h4>
                    <ul>
                        <li>Wycieczkę pieszą na Halę Barankową – spotkanie z Góralami</li>
                        <li>Wycieczkę pieszą w pasmo Babiej Góry (1725m npm) -Pasmo Policy (1369m npm), Pasmo Kolisty
                            Groń (1118m npm), Schronisko Markowe Szczawiny, Diablak (1725 m npm (koszt przewodnika ok.
                            200 zł od grupy)
                        </li>
                        <li>Wycieczkę pieszą do Ośrodka Edukacyjnego BPN i Skansenu „Markowe Rówienki”</li>
                        <li>Wycieczki piesze i rowerowe po terenie Zawoi, koszt wypożyczenia roweru 25 zł za szt. Na
                            cały dzień
                        </li>
                        <li>Zajęcia nauki jazdy konnej – 50 zł godzina, stadnina koni 1km od ośrodka</li>
                        <li>Możliwość realizacji w Ośrodku zajęć rękodzielniczych – koszt 2-6 zł od osoby</li>
                        <li>Wycieczkę autokarową do Orawskiego Parku Etnograficznego - Skansen w Zubrzycy – autokar 250
                            zł
                        </li>
                        <li>Wycieczkę autokarową do Suchej Beskidzkiej, mały Wawel, Grota solno-jodowa, basen – autokar
                            250 zł
                        </li>
                        <li>Wycieczka autokarowa: Wadowice – Kraków lub Kopalnia soli w Wieliczce – autokar 650 zł</li>
                        <li>Wycieczka autokarowa: Zakopane, Aquapark – autokar 600 zł</li>
                        <li>Wycieczka autokarowa: Słowacja Oravice lub Bukowina Tatrzańska - źródła termalne– autokar
                            650 zł
                        </li>
                    </ul>
                    <br/>

                    <div align="center">
                        <h6>Terminy pobytów do szczegółowego ustalenia.</h6>
                        <h6>Ceny przy grupach i dłuższych pobytach do negocjacji!</h6>

                        <p>&nbsp;</p>
                        <h5>OW ZAWOJANKA – NIEOGRANICZONA PRZESTRZEŃ WYPOCZYNKU!</h5>
                        <h5>ZAPRASZAMY SERDECZNIE !</h5>
                    </div>
                </div>
            </div>
            <!-- google_ad_section_end -->
        </div>
    </div>
</div>
           