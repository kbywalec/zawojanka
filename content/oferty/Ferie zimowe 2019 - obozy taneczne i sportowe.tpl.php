<div class="main section" id="main">
<div class="widget Blog" id="Blog1">
<div class="blog-posts hfeed">
<div class="post hentry uncustomized-post-template">
<h2 class="post-title entry-title">Ferie zimowe 2019 - obozy taneczne i sportowe</h2>

<div class="entry"><!--<img border="0" src="media/images/obiekt.jpg" align="left" hspace="10"/>-->
<div align="center"><h7>Położony u stóp Królowej Beskidów – Babiej Góry (1725m npm)<br />
Ośrodek Wypoczynkowy ZAWOJANKA zaprasza na pobyt i wypoczynek w ciągu całego roku dzieci i młodzieży </h7></div>
&nbsp;

<h4>☺ ZAPEWNIAMY:</h4>

<ul>
	<li><strong>Komfortowe warunki do zajęć tanecznych i wypoczynku.</strong></li>
	<li>Poznanie osobliwości przyrodniczych Babiogórskiego Parku Narodowego i ciekawej kultury regionalnej</li>
	<li>Program kulturalny: ogniska z pieczeniem kiełbasy, dyskoteki,&nbsp; konkursy z nagrodami
	<p>&nbsp;</p>
	</li>
</ul>
&nbsp;

<h4>☺ ŚWIADCZENIA:</h4>

<table align="center" border="0" cellspacing="3" width="100%">
	<tbody>
		<tr>
			<td><img src="/media/galeria/zimowisko1.jpg" /></td>
			<td><img src="/media/galeria/zimowisko2.jpg" /></td>
		</tr>
	</tbody>
</table>

<ul>
	<li>Zakwaterowanie w pokojach 2,3,4-os. z łazienkami</li>
	<li>Wyżywienie: 3 posiłki lub wg indywidualnych ustaleń</li>
	<li>Sale dydaktyczne (2 szt), +&nbsp; 3 sale taneczne lub na zajęcia sportowe w tym 2 z parkietem i lustrami do zajęć tanecznych 9m x 15m (w cenie pobytu)</li>
	<li>Kadra pedagogiczna ( bezpłatnie 1 nauczyciel na 15 uczniów)</li>
	<li>Imprezy rekreacyjno – sportowe na własnym kompleksie na terenie ośrodka, basen letni.</li>
	<li>Możliwość zapewnienia odpłatnie całodobowej opieki medycznej.</li>
	<li>Do dyspozycji przewodnik na piesze wycieczki (1 raz w każdym tygodniu pobytu)</li>
</ul>
&nbsp;

<h4>☺ PROPONUJEMY DODATKOWO:</h4>

<ul>
	<li>Wycieczkę pieszą na Halę Barankową –&nbsp; spotkanie z Góralami</li>
	<li>Wycieczkę pieszą w pasmo Babiej Góry - (koszt przewodnika ok. 200 zł od grupy)</li>
	<li>Wycieczkę pieszą lub rowerową do Ośrodka Edukacyjnego BPN i Skansenu</li>
	<li>Wycieczki piesze i rowerowe po terenie Zawoi, koszt wypożyczenia roweru 25 zł za szt na cały dzień</li>
	<li>Zajęcia nauki jazdy konnej – 50 zł godzina, stadnina koni 1 km od ośrodka</li>
	<li>Możliwość realizacji na miejscu w Ośrodku zajęć rękodzielniczych. – koszt 2-6 zł / uczestnik</li>
	<li>Wycieczkę autokarową do Orawskiego Parku Etnograficznego - Skansen w Zubrzycy – autokar 300 zł</li>
	<li>Wycieczkę autokarową do Suchej Beskidzkiej,&nbsp; mały Wawel, Grota solno-jodowa, basen – autokar 300 zł</li>
	<li>Wycieczkę autokarową: Zakopane – autokar 700 zł</li>
	<li>Wycieczkę autokarową: Oravice Słowacja lub Bukowina Tatrzańska– źródła termalne.
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Wstępy podczas wycieczek autokarowych ok. 20 zł / uczestnik.&nbsp;&nbsp;<br />
	<br />
	Możliwość rezerwacji hali sportowej na zajęcia taneczne ( 600m od ośrodka, wymiary 14 x 26 m, koszt 40 zł / godz).<br />
	Możliwość korzystania bezpłatnie z kompleksu „Orlik” - 600m od ośrodka.&nbsp;&nbsp; &nbsp;</p>

	<p>&nbsp;</p>
	</li>
</ul>
&nbsp;

<h4>☺ KOSZT POBYTU:</h4>

<strong>Przy grupach zorganizowanych kwoty ustalane indywidualnie</strong></p>
<h7>Terminy pobytów do szczegółowego ustalenia. Grupy zorganizowane - możliwość negocjacji cen!</h7>

<p>&nbsp;</p>

<h3 align="center">&nbsp;</h3>

<h5>Zapraszamy !</h5>
</div>
</div>
<!-- google_ad_section_end --></div>
</div>
</div>
