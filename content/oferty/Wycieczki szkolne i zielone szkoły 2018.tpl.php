<h2 align="center">Wycieczki szkolne i zielone szkoły 2018</h2>

<h2 align="center">Wiosna 2018r</h2>

<h4 align="center"><em>Położony u stóp Królowej Beskidów – Babiej Góry (1725m npm)</em></h4>

<h4><em>Ośrodek Wypoczynkowy </em><em>„ZAWOJANKA” </em><em> oferuje pobyt dla dzieci i młodzieży&nbsp;</em></h4>

<h4 align="center"><em>w ramach wycieczek szkolnych, zielonych szkół ! </em></h4>

<p><strong><span style="font-size:14px;">☺ <em>ZAPEWNIAMY:</em></span></strong></p>

<ul>
	<li>
	<p><span style="font-size:14px;">Wypoczynek wśród pięknych górskich krajobrazów w ciszy i spokoju</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Poznanie osobliwości przyrodniczych Babiogórskiego Parku Narodowego – Światowego Rezerwatu Biosfery</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Poznanie ciekawej kultury regionalnej</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Program kulturalny: ogniska z pieczeniem kiełbasy, dyskoteki, konkursy z nagrodami</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Przewodnika górskiego w ramach kosztów pobytu (do uzgodnienia).</span></p>
	</li>
	<li>
	<p>&nbsp;</p>
	</li>
</ul>

<p><strong><span style="font-size:14px;">☺ <em>ŚWIADCZENIA:</em></span></strong></p>

<ul>
	<li>
	<p><span style="font-size:14px;">Zakwaterowanie w pokojach 2,3,4 -os. z łazienkami i tv – łącznie 170 miejsc noclegowych</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wyżywienie: 3 posiłki + podwieczorek lub wg indywidualnych ustaleń.</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Sale dydaktyczne, 3 sale ( 9m x14m) do zajęć np. tanecznych( lustra), sportowych (parkiet), edukacyjnych</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Kadra pedagogiczna ( bezpłatnie 1 nauczyciel na 12 uczniów)</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Imprezy rekreacyjno – sportowe na własnym kompleksie na terenie ośrodka</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Możliwość zapewnienia odpłatnie całodobowej opieki medycznej.</span></p>
	</li>
	<li>
	<p>&nbsp;</p>
	</li>
</ul>

<p><span style="font-size:14px;"><strong>☺ <em>PROPONUJEMY</em><em>:</em></strong></span></p>

<ul>
	<li>
	<p><span style="font-size:14px;">Wycieczkę do Centrum Górskiego „Korona Ziemi” - 200 m od ośrodka</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wycieczkę pieszą na Halę Barankową – spotkanie z Góralami</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wycieczkę pieszą w pasmo Babiej Góry (1725m npm)</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wycieczkę pieszą do Ośrodka Edukacyjnego BPN i Skansenu „Markowe Rówienki”</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wycieczki piesze i rowerowe po terenie Zawoi, koszt wypożyczenia roweru 25 zł za szt na cały dzień</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wycieczka do gospodarstwa agroturystycznego - „Park Czarnego Daniela”.</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Zajęcia nauki jazdy konnej – 50 zł godzina, stadnina koni 1 km od ośrodka</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Zielony kulig na wozach – 20 zł</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Możliwość realizacji na miejscu w Ośrodku zajęć rękodzielniczych. – koszt 5-7 zł / uczestnik</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wycieczkę autokarową do Orawskiego Parku Etnograficznego - Skansen w Zubrzycy – autokar 250 zł</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wycieczkę autokarową do Suchej Beskidzkiej, mały Wawel, Grota solno-jodowa, basen – autokar 300 zł</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wycieczka autokarowa: Zakopane, Bukowina Tatrzańska – baseny termalne – autokar 700 zł</span></p>
	</li>
	<li>
	<p><span style="font-size:14px;">Wycieczka autokarowa Inwałd k. Wadowic – Park Miniatur – autokar 650 zł</span></p>
	</li>
	<li>
	<p>&nbsp;</p>
	</li>
</ul>

<p style="margin-left:2.54cm;">&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p align="center"><span style="font-size:12px;"><strong>Terminy pobytów do szczegółowego ustalenia. Ceny przy dłuższych pobytach do negocjacji!</strong></span></p>

<p align="center">&nbsp;</p>

<p align="center"><span style="font-size:12px;"><strong>Zapraszamy </strong>!</span></p>
