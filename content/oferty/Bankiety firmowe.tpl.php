                <div class="main section" id="main">
                  <div class="widget Blog" id="Blog1">
                    <div class="blog-posts hfeed">
                      <div class="post hentry uncustomized-post-template">
                        <h2 class="post-title entry-title">Bankiety firmowe</h2>
                        <div class="entry">


                            <p>Ośrodek Wypoczynkowy Zawojanka to największa baza noclegowa położona w malowniczej miejscowości Zawoja u st&oacute;p Babiej G&oacute;ry. To idealne miejsce na zorganizowanie firmowej imprezy integracyjnej czy wyjazdu szkoleniowego.</p>

                            <p>Dysponujemy 170 miejscami noclegowymi w komfortowych pokojach z łazienkami. Zapewniamy całodzienne wyżywienie dostosowane do potrzeb i oczekiwań naszych Gości. Na terenie naszego Ośrodka znajduje się boisko, letni basen, miejsce na grill i ognisko, st&oacute;ł do ping-ponga oraz wifi.</p>

                            <p>Dużym atutem są pełnowymiarowe sale lustrzane (9 x 14m), na kt&oacute;rych z powodzeniem można przeprowadzać zajęcia taneczne, sportowe, imprezy lub organizować szkolenia i konferencje.&nbsp;</p>

                            <p>PRZYKŁADOWE ATRAKCJE PROGRAMOWE NA CZAS POBYTU:</p>

                            <ul>
                                <li>biesiada g&oacute;ralska przy ognisku, w plenerze lub w jednej z wielu sal rekreacyjnych ośrodka wraz w muzykowaniem i występami miejscowych zespoł&oacute;w folklorystycznych</li>
                                <li>kulig (na saniach lub wozach w zależności od pory roku)</li>
                                <li>zajęcia i nauka jazdy konnej &ndash; wyjazdy rekreacyjno -krajoznawcze w siodle</li>
                                <li>wycieczki tematyczne z przewodnikiem po okolicy, pieszo i rowerami</li>
                                <li>wycieczki krajoznawcze i przejażdżki rekreacyjne quadami &ndash; sprawdzian umiejętności lub nauka jazdy na profesjonalnym torze, opieka i pomoc instruktora</li>
                                <li>park linowy &ndash; zmagania z własną siłą bądź słabością</li>
                                <li>paintball &ndash; zajęcia indywidualne oraz w grupach zorganizowanych</li>
                                <li>escape room</li>
                            </ul>

                            <p>Dysponujemy dużym doświadczeniem w organizowaniu imprez integracyjnych, wyjazd&oacute;w szkoleniowych czy konferencji. Przyjeżdżając do nas mają Państwo gwarancję udanego pobytu i pysznego jedzenia :)</p>

                            <p>Koszty pobytu ustalane są indywidualnie.</p>

                            <p>ZAPRASZAMY DO KONTAKTU!</p>




                          </div>
                        </div>
                      </div>
                      <!-- google_ad_section_end -->
                    </div>
                  </div>
                </div>
           