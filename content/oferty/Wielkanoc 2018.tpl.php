                <div class="main section" id="main">
                  <div class="widget Blog" id="Blog1">
                    <div class="blog-posts hfeed">
                      <div class="post hentry uncustomized-post-template">
                        <h2 class="post-title entry-title">WIELKANOC 2018</h2>
                        <div class="entry">
                          <img border="0" src="/media/images/wielkanoc2018b.jpg" align="center" style="width: 100%; margin-bottom: 0;"/>
                                                       <a href="https://www.facebook.com/events/218123708750340/"><h5 class="text-center" style="background: #3b5998; color: #ffffff; text-align: center;">Dołącz do wydarzenia na Facebook</h5></a>



                            <p class="text-center"><strong>Masz dość świątecznego zgiełku?</strong></p>
                            <p class="text-center"><strong>Wolisz odpocząć, niż piec i gotować?</strong></p>
                            <p class="text-center"><strong>Masz ochotę na górskie wędrówki, klimatyczne ogniska i regionalne potrawy?</strong></p>
<br>
                            <h5>NIE TRAFISZ LEPIEJ :)</h5>
                            <br>
                            <p>Zapraszamy do spędzenia pięknych, wiosennych Świąt Wielkanocnych w Zawoi! Rodzinne chwile w Zawojance zagwarantują Wam wspaniałe wspomnienia na długo.</p>
                            <br>
                            <p><strong>Przygotowaliśmy dla Państwa specjalne, świąteczne pakiety:</strong></p>
                            <ul>
                                <li>- 30.03.2018r. - 03.04.2018r. - 360 zł</li>
                                <li>- 31.03.2018r. - 02.04.2018r. - 250 zł</li>
                            </ul>
                            <br>
                            <p>Czas pobytu można dowolnie modyfikować.</p>
                            <br>
                            <p><strong>Oferujemy:</strong></p>
                            <ul>
                                <li>pyszne, świąteczne posiłki</li>
                                <li>ognisko z degustacją regionalnych wyrobów</li>
                                <li>degustacja nalewek</li>
                                <li>spotkanie z gawędziarzem</li>
                                <li>warsztaty rękodzielnicze dla dzieci i zainteresowanych dorosłych</li>
                                <li>święcenie pokarmów</li>
                                <li>późne wykwaterowanie</li>
                                <li>pobyt dziecka na preferencyjnych warunkach (dzieci do 8 lat -50%)</li>
                                <li>i wiele innych... :)</li>
                            </ul>
                            <br><br>
<p>Wszystkich zainteresowanych zapraszamy do kontaktu!</p>
<p>tel. 577 174 634</p>
<p>biuro@zawojanka.com</p>
<p class="text-center">ZAPRASZAMY!</p>








                          </div>
                        </div>
                      </div>
                      <!-- google_ad_section_end -->
                    </div>
                  </div>
                </div>
           