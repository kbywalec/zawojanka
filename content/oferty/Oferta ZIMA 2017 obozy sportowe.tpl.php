<div class="main section" id="main">
<div class="widget Blog" id="Blog1">
<div class="blog-posts hfeed">
<div class="post hentry uncustomized-post-template">
<h2 class="post-title entry-title">ZIMOWISKO&nbsp;&nbsp;&nbsp; Zawoja&nbsp; 2017r</h2>

<div class="entry"><!--<img border="0" src="media/images/obiekt.jpg" align="left" hspace="10"/>-->
<div align="center"><h7>Położony u stóp Królowej Beskidów – Babiej Góry (1725m npm) Ośrodek Wypoczynkowy „ZAWOJANKA”&nbsp; oferuje pobyt dla dzieci i młodzieży w ramach zimowiska, obozów tanecznych, sportowych i narciarskich!</h7></div>
&nbsp;

<h4>☺ ZAPEWNIAMY:</h4>

<ul>
	<li>Poznanie osobliwości i zimowych uroków Babiogórskiego Parku Narodowego</li>
	<li>Doskonalenie i nauka jazdy na nartach i snowboardzie.</li>
	<li>Poznanie ciekawej kultury regionalnej</li>
	<li>Program kulturalny: kuligi z ogniskiem i pieczeniem kiełbasy, dyskoteki,&nbsp; konkursy z nagrodami</li>
	<li>Na miejscu w ośrodku w ramach kosztów pobytu nieograniczony dostęp do 2 sal z parkietem o wymiarach 9 x 14 m – komfortowe warunki do zajęć tanecznych (lustra) i zajęć sportowych.
	<p>&nbsp;</p>
	</li>
</ul>

<table align="center" border="0" cellspacing="3" width="100%">
	<tbody>
		<tr>
			<td><img src="/media/galeria/zimowisko1.jpg" /></td>
			<td><img alt="" src="/media/galeria/zimowisko2.jpg" /></td>
		</tr>
	</tbody>
</table>

<h4>☺ ŚWIADCZENIA:</h4>

<ul>
	<li>Zakwaterowanie w pokojach 2,3,4-os. z łazienkami i TV</li>
	<li>Wyżywienie: 3 posiłki lub wg odrębnych ustaleń</li>
	<li>Kadra pedagogiczna ( bezpłatnie 1 osoba na 15 miejsc pełnopłatnych)</li>
	<li>Możliwość zapewnienia dowozu busami do wyciągów narciarskich.</li>
	<li>Możliwość kompleksowej organizacji zimowiska, ze sprzętem i instruktorami narciarskimi.</li>
</ul>
&nbsp;

<h4>☺ PROPONUJEMY:</h4>

<ul>
	<li>Wycieczkę do Centrum Górskiego „Korona Ziemi” - 200m od ośrodka</li>
	<li>Wycieczkę pieszą do Ośrodka Edukacyjnego BPN.</li>
	<li>Możliwość realizacji na miejscu w Ośrodku zajęć rękodzielniczych – koszt 6 zł / uczestnik</li>
	<li>Wycieczkę autokarową do Orawskiego Parku Etnograficznego - Skansen w Zubrzycy – autokar 300 zł</li>
	<li>Wycieczkę autokarową do Suchej Beskidzkiej,&nbsp; mały Wawel, Grota solno-jodowa, basen – autokar 300 zł</li>
	<li>Wycieczkę autokarową: Wadowice, muzeum JPII, Inwałd – Park miniatur – autokar – 600 zł</li>
	<li>Wycieczkę autokarową: Zakopane –Aquapark -&nbsp; autokar 700 zł</li>
	<li>Wycieczkę autokarową: Oravice Słowacja – źródła termalne – autokar 750 zł</li>
	<li>Wstępy podczas wycieczek autokarowych ok. 20 zł / uczestnik, kulig na saniach 15 zł / uczestnik. Skansen w Zubrzycy – 8 zł, Muzeum BPN – 5 zł / uczestnik, Park Miniatur – 15 zł, Gawędziarze Góralscy – 300 zł / występ 2 godz.
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
	<strong>Wyciągi Narciarskie:</strong><br />
	- Kompleks wyciągów orczykowych - Zawoja Czatoża (1,5 km od ośrodka)<br />
	- I Wyciąg duży: (400m),&nbsp; 4 wyciągi mniejsze ( różne stopnie zaawansowania)<br />
	- II Wyciągi duże (410m) i&nbsp; (150m) - niskie ceny, do negocjacji<br />
	&nbsp; Kolej krzesełkowa „Mosorny Groń” – 3 km od ośrodka<br />
	- czteroosobowe krzesła, długość trasy ok 1450 m<br />
	- wyciąg orczykowy (60m-dla dzieci) i&nbsp; wyciąg talerzykowy (330m) dla szkółek narciarskich&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<br />
	-Lodowisko – 500 m od ośrodka – bilety 2 i 3 zł, na miejscu wypożyczalnia łyżew &nbsp;</p>

	<p>&nbsp;</p>
	</li>
</ul>
&nbsp;

<h4>☺ KOSZT POBYTU w turnusach min 5 dniowych: :</h4>
	<p><strong>Przy grupach zorganizowanych kwoty ustalane indywidualnie</strong></p>

<!--<p><strong>Od 80 zł</strong> osoba dorosła. - pobyt indywidualny<br />-->
<!--&nbsp;&nbsp;&nbsp;</p>-->
<h7>Większe grupy zorganizowane od 50 osób – cena dostosowana indywidualnie, zależnie od wielkości i ilości dni pobytu !!!</h7>

<p>&nbsp;</p>

<h3 align="center">&nbsp;</h3>

<h5>Zapraszamy !</h5>
</div>
</div>
<!-- google_ad_section_end --></div>
</div>
</div>
