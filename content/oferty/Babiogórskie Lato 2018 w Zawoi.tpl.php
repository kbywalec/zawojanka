<h1 align="center" class="western"><i><b>Babiogórskie lato 2018  Zawoi</b></i></h1>

<p align="center">&nbsp;</p>

<p align="center"><em><strong>Położony u stóp Królowej Beskidów – Babiej Góry (1725m npm)</strong></em></p>

<p align="center"><em><strong>Ośrodek Wypoczynkowy „ZAWOJANKA” zaprasza na </strong></em></p>

<p>&nbsp;</p>

<h3 align="center"><i><b>turnus wypoczynkowo-rekreacyjny!</b></i></h3>

<h4>&nbsp;</h4>

<h4 style="text-align: left; margin-left: 80px;"><span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: 16px; white-space: pre-wrap; line-height: 1.2; background-color: transparent;">Terminy &nbsp;: </span></h4>

<ol style="margin-left: 120px;">
	<li>
	<h4 style="text-align: left;"><span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: 16px; white-space: pre-wrap; line-height: 1.2; background-color: transparent;">1. od &nbsp;4 lipca 2016r poniedziałek do 10 lipca 2016r sobota </span></h4>
	</li>
	<li>
	<h4 style="text-align: left;"><span style="font-size: 16px; font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;">2. od 11 lipca 2016r poniedziałek do 17 lipca 2016r sobota </span></h4>
	</li>
	<li>
	<h4 style="text-align: left;"><span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: 16px; white-space: pre-wrap; line-height: 1.2; background-color: transparent;">3. od 19 sierpnia 2016r - piątek – do 25 sierpnia 2016r – czwartek</span></h4>
	</li>
</ol>

<div>&nbsp;</div>

<p>&nbsp;</p>

<p><strong>Turnusy rozpoczynają się obiadokolacją, a kończą śniadaniem.</strong></p>

<p>&nbsp;</p>

<p>☺ <i><b>ŚWIADCZENIA:</b></i></p>

<ul>
	<li>
	<p>Zakwaterowanie w pokojach 2, 3, 4-os. z łazienkami i tv</p>
	</li>
	<li>
	<p>Wyżywienie: 2 posiłki / dziennie, śniadania i obiadokolacje</p>
	</li>
	<li>
	<p>Ognisko z pieczeniem kiełbasek</p>
	</li>
	<li>
	<p>Piesze wycieczki z przewodnikiem po najbliższej okolicy.</p>
	</li>
	<li>
	<p>Możliwość codziennego korzystania z letniego basenu kąpielowego 6 x 3 m</p>
	</li>
	<li>
	<p>Własny teren rekreacyjny do gier i zabaw na świeżym powietrzu</p>
	</li>
</ul>

<p class="western" style="margin-left: 3.49cm; text-indent: -0.95cm; margin-bottom: 0cm">&nbsp;</p>

<p>☺ <i><b>PROPONUJEMY za dodatkową odpłatnością</b></i><i><b>:</b></i></p>

<ul>
	<li>
	<p>Przejazd wozami – zielony kulig – 15 zł</p>
	</li>
	<li>
	<p>Wycieczkę pieszą na Halę Barankową – spotkanie z Góralami – ok. 10 zł</p>
	</li>
	<li>
	<p>Wycieczkę pieszą w pasmo Babiej Góry (1725m npm) – ok. 15 zł</p>
	</li>
	<li>
	<p>Wycieczkę pieszą do Ośrodka Edukacyjnego BPN i Skansenu „Markowe Rówienki” – 8 zł</p>
	</li>
	<li>
	<p>Wycieczki piesze i rowerowe po terenie Zawoi, koszt wypożyczenia roweru 25 zł za szt na cały dzień</p>
	</li>
	<li>
	<p>Zajęcia nauki jazdy konnej – stadnina koni 1 km od ośrodka, ceny do indywidualnego ustalenia</p>
	</li>
	<li>
	<p>Quady – 100 zł / godz, Paintaball – od 35 zł</p>
	</li>
	<li>
	<p>Wycieczkę autokarową do Orawskiego Parku Etnograficznego - Skansen w Zubrzycy – 30 zł</p>
	</li>
	<li>
	<p>Wycieczkę autokarową do Suchej Beskidzkiej, mały Wawel, Grota solno-jodowa, – 50 zł</p>
	</li>
	<li>
	<p>Wycieczka autokarowa: Oravice Słowacja – baseny termalne – 45 zł (bez biletów wstępu na termy)</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>☺ <i><b>KOSZTY POBYTU:</b></i></p>

<p class="western" style="margin-left: 2.5cm; margin-bottom: 0cm"><b>350 zł – pobyt za pełen pakiet </b></p>

<p><b>70</b><b> zł - </b>osobodoba przy pobytach minimum 3 lub 4 dniowych w w/w terminie.</p>

<p>zniżki dla dzieci – udzielane indywidualnie w zależności od wieku do 10 %</p>

<p>Rodziny: odpłatność za 2 osoby dorosłe + 1 dziecko ze zniżką 10%, 2-gie dziecko 20% zniżki (wiek do 10 lat)</p>

<p>&nbsp;</p>

<p align="center"><b>Zapraszamy </b>!</p>
