<div class="main section" id="main">
<div class="widget Blog" id="Blog1">
<div class="blog-posts hfeed">
<div class="post hentry uncustomized-post-template">
<h2 class="post-title entry-title">Biesiady w Zawoi 2016</h2>

<div class="entry"><h7>Położony u stóp Królowej Beskidów – Babiej Góry (1725 m npm)<br />
Ośrodek Wypoczynkowy ZAWOJANKA oferuje atrakcyjny<br />
wypoczynek weekendowy, wyjazdy integracyjne !</h7><br />
&nbsp;
<h4>☺ ZAPEWNIAMY:</h4>

<ul>
	<li>Poznanie osobliwości przyrodniczych Babiogórskiego Parku Narodowego – Światowego Rezerwatu Biosfery</li>
	<li>Poznanie ciekawej kultury regionalnej, naszych zwyczajów, codziennego życia, smacznej Zawojskiej kuchni.</li>
	<li>Wypoczynek wśród pięknych górskich krajobrazów w ciszy i spokoju lub czynne uczestnictwo w naszych propozycje programowe.</li>
</ul>
&nbsp;

<h4>☺ ŚWIADCZENIA:</h4>

<ul>
	<li>Zakwaterowanie w pokojach 2, 3 lub 4 -osobowych z łazienkami i TV.</li>
	<li>Wyżywienie: wg ustaleń, zawsze dostosowane do potrzeb i oczekiwań.</li>
</ul>
&nbsp;

<h4>☺ PRZYKŁADOWE ATRAKCJE PROGRAMOWE NA CZAS POBYTU:</h4>

<ul>
	<li>biesiada góralska przy ognisku, w plenerze lub w jednej z wielu sal rekreacyjnych ośrodka wraz w muzykowaniem i występami miejscowych zespołów folklorystycznych</li>
	<li>kulig (na saniach lub wozach w zależności od pory roku)</li>
	<li>zajęcia i nauka jazdy konnej – wyjazdy rekreacyjno -krajoznawcze w siodle</li>
	<li>wycieczki tematyczne z przewodnikiem po okolicy, pieszo i rowerami</li>
	<li>wycieczki krajoznawcze i przejażdżki rekreacyjne quadami – sprawdzian umiejętności lub nauka jazdy na profesjonalnym torze, opieka i pomoc instruktora, (uwaga nowość ubezpieczenie NNW)</li>
	<li>surwiwal pojazdami z napędem 4x4 – specjalny tor i instruktorzy</li>
	<li>loty widokowe i wyczynowe na paralotni – niezapomniane wrażenia, dokumentacja lotu w formie zdjęć i dvd</li>
	<li>park linowy – zmagania z własną siłą bądź słabością</li>
	<li>paintball – zajęcia indywidualne oraz w grupach zorganizowanych</li>
	<li>kasyno – pokazy i gra rekreacyjna na miejscu w ośrodku</li>
</ul>
<br />
<h7>Proponujemy dodatkowo do realizacji podczas pobytu:</h7>

<ul>
	<li>Wycieczki tematyczne: szlakiem regionalnej kuchni, przyrodnicze wojaże, zbójecki szlak.</li>
	<li>Wycieczkę autokarową do Orawskiego Parku Etnograficznego - Skansen w Zubrzycy</li>
	<li>Wycieczkę autokarową do Suchej Beskidzkiej, mały Wawel, Grota solno-jodowa, basen</li>
	<li>Wycieczka autokarowa: Wadowice – Kraków lub Kopalnia soli w Wieliczce</li>
	<li>Wycieczka autokarowa: Zakopane, baseny termalne Szaflary</li>
	<li>Wycieczka autokarowa: Oravice Słowacja – baseny termalne, gorące źródła</li>
</ul>
&nbsp;

<h4>☺ KOSZTY POBYTU:</h4>

<p><strong>Przy grupach zorganizowanych kwoty ustalane indywidualnie</strong></p>
<strong>Terminy pobytów do szczegółowego ustalenia. </strong><br />
Możliwość obniżenia kosztów pobytu i zmiany programu, szczególnie dla grup zorganizowanych !!!</p>
<h7>Ośrodek Wypoczynkowy „Zawojanka” w Zawoi</h7><br />
Położony w pobliżu Babiogórskiego Parku Narodowego, w sąsiedztwie lasu, strumienia, szlaków turystycznych, rowerowych, stadniny koni. Teren wokół ośrodka bezpieczny, monitorowany, ogrodzony 1,5 ha kompleks z własnym terenem rekreacyjno-sportowym, placem zabaw dla dzieci, boiskami do gier i zajęć na świeżym powietrzu, parkingiem.<br />
Jesteśmy największą bazą turystyczno-hotelarską w Zawoi, dysponujemy 170 miejscami noclegowymi w pokojach z łazienkami. Gwarantujemy smaczną regionalną kuchnię.
<p>&nbsp;</p>

<h5><em>Zapraszamy !</em></h5>
</div>
</div>
</div>
</div>
</div>
