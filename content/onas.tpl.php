
                <div class="main section" id="main">
                  <div class="widget Blog" id="Blog1">
                    <div class="blog-posts hfeed">
                      <div class="post hentry uncustomized-post-template">
                        <h2 class="post-title entry-title">O nas</h2>
                        <div class="entry">
                          
                          <p>Jesteśmy grupą pasjonatów kochających góry, świeże powietrze, dobrą kuchnię oraz…wypoczynek. Znamy się doskonale na naszym fachu oraz dysponujemy ogromnym doświadczeniem ponieważ od blisko 10 lat gościmy w naszym ośrodku <u><strong>ok. 5000 osób rocznie!!!</strong></u></p>
                          <p>ZAWOJANKA to nasze oczko w głowie, nasz skarb, którym chcemy się z Państwem podzielić. Żaden inny ośrodek w Zawoi nie posiada tak dużej liczby pokoi jak również takich warunków do aktywnego wypoczywania:</p>
                          <ul>
                            <li>2 sale z parkietem o wymiarach 9m x 14m każda, idealne do zajęć tanecznych, ćwiczeń i treningów,</li>
                            <li>Sala dyskotekowa,</li>
                            <li> Boiska do koszykówki i siatkówki,</li>
                            <li> Stół do ping-ponga,</li>
                            <li> Plac zabaw dla dzieciaków,</li>
                            <li>Miejsce na grill i ognisko,</li>
                            <li>W budowie korty tenisowe.</li>
                          </ul>
                          <p>Przygotowaliśmy dla Państwa 46 pokoi: 2, 3, 4 i 5  – osobowe, mogące pomieścić łącznie 170 osób! W każdym pokoju znajduje się <u><strong>łazienka oraz telewizor</strong></u>. Do Państwa dyspozycji są także sale do ćwiczeń, jadalnia oraz inne atrakcje, o których mowa powyżej - udostępniane bezpłatnie. <strong>ZAWOJANKA</strong> zapewnia także zawsze świeżą i smaczną kuchnię regionalną. Przed budynkiem znajduje się parking dla naszych Gości.</p>
                          <p>Cały ośrodek łącznie z terenem rekreacyjnym, placem zabaw i parkingiem jest ogrodzony i monitorowany.</p>
                          <p>Zapewniamy Państwu możliwość obcowania oraz poznania przyrody Babiogórskiego Parku Narodowego (www.bgpn.pl) jak i również tutejszej kultury i kuchni  Gwarantujemy wypoczynek w ciszy i spokoju, a dla szczęśliwców - niesamowitą panoramę całego masywu Babiej Góry z okien pokoju!</p>
                          <br /><h5 align="center">OW ZAWOJANKA – NIEOGRANICZONA PRZESTRZEŃ WYPOCZYNKU!</h5>
                          <h5 align="center">ZAPRASZAMY SERDECZNIE!</h5>
                        </div>
                      </div>
                     
                    </div>
                  </div>
                </div>
             