
                <div class="main section" id="main">
                  <div class="widget Blog" id="Blog1">
                    <div class="blog-posts hfeed">
                      <div class="post hentry uncustomized-post-template">
                        <h2 class="post-title entry-title">Wycieczki szkolne, zielone szkoły - Wiosna 2013 r.</h2>
                        <div class="entry">
                         
                         
                          <h7>Położony u stóp Królowej Beskidów – Babiej Góry (1725m npm)<br>
                            Ośrodek Wypoczynkowy ZAWOJANKA oferuje pobyt dla dzieci i młodzieży<br>
                            w ramach wycieczek szkolnych, zielonych szkół ! </h7><br /><br />
                          <h4> ☺ ZAPEWNIAMY:</h4>
                          <ul>
                            <li> Wypoczynek wśród pięknych górskich krajobrazów w ciszy i spokoju</li>
                            <li> Poznanie osobliwości przyrodniczych Babiogórskiego Parku Narodowego – Światowego Rezerwatu Biosfery</li>
                            <li> Poznanie ciekawej kultury regionalnej</li>
                            <li> Program kulturalny: ogniska z pieczeniem kiełbasy, dyskoteki,  konkursy z nagrodami</li>
                            <li> Przewodnika górskiego w ramach kosztów pobytu (do uzgodnienia).<br>
                            </li>
                          </ul><br />
                          <h4> ☺ ŚWIADCZENIA:</h4>
                          <ul>
                            <li>Zakwaterowanie w pokojach 2,3,4 -os. z łazienkami i tv – łącznie 180 miejsc noclegowych</li>
                            <li> Wyżywienie: 3 posiłki  + podwieczorek lub wg indywidualnych ustaleń.</li>
                            <li> Sale dydaktyczne, 3 sale ( 9m x14m) do zajęć np. tanecznych( lustra), sportowych (parkiet), edukacyjnych</li>
                            <li> Kadra pedagogiczna ( bezpłatnie 1 nauczyciel na 12 uczniów)</li>
                            <li> Imprezy rekreacyjno – sportowe na własnym kompleksie na terenie ośrodka</li>
                            <li> Możliwość zapewnienia odpłatnie całodobowej opieki medycznej.</li>
                          </ul>
                          <br>
                          <h4> ☺ PROPONUJEMY:</h4>
                          <ul>
                            <li>Wycieczkę pieszą na Halę Barankową –  spotkanie z Góralami</li>
                            <li> Wycieczkę pieszą w pasmo Babiej Góry  (1725m npm) </li>
                            <li> Wycieczkę pieszą do Ośrodka Edukacyjnego BPN i Skansenu „Markowe Rówienki”</li>
                            <li> Wycieczki piesze i rowerowe po terenie Zawoi, koszt wypożyczenia roweru 25 zł za szt na cały dzień</li>
                            <li> Wycieczka do gospodarstwa agroturystycznego - „Park Czarnego Daniela”.</li>
                            <li> Zajęcia nauki jazdy konnej – 50 zł godzina, stadnina koni 1 km od ośrodka</li>
                            <li> Zielony kulig na wozach – 15 zł </li>
                            <li> Możliwość realizacji na miejscu w Ośrodku zajęć rękodzielniczych. – koszt 2-6 zł / uczestnik</li>
                            <li> Wycieczkę autokarową do Orawskiego Parku Etnograficznego - Skansen w Zubrzycy – autokar 250 zł</li>
                            <li> Wycieczkę autokarową do Suchej Beskidzkiej,  mały Wawel, Grota solno-jodowa, basen – autokar 250 zł</li>
                            <li> Wycieczka autokarowa: Zakopane – autokar 650 zł</li>
                            <li> Wycieczka autokarowa Inwałd k. Wadowic – Park Miniatur – autokar 650 zł</li>
                            <li> Wycieczka autokarowa: Bukowina Tatrzańska – baseny termalne – autokar 650 zł</li>
                          </ul>
                         <br /> <h4>☺ KOSZT POBYTU:</h4>
                          <ul>
                            <strong>50 zł</strong> osobodoba przy pobytach 1-4 dniowych<br>
                            <strong>45 zł</strong> osobodoba przy pobytach dłuższych
                          </ul>
                          <p>Wstępy podczas wycieczek autokarowych ok. <strong>17 zł</strong> / uczestnik.<br>
                            Skansen w Zubrzycy – <strong>7 zł</strong>, Muzeum BPN – <strong>5 zł </strong>/ uczestnik</p>
                          <h4> Terminy pobytów do szczegółowego ustalenia. Ceny przy grupach i dłuższych pobytach do negocjacji! <br />
                            Uwaga! Możliwość wydłużenia terminów płatności lub zapłaty w dogodnych ratach! <br>
                          </h4>
                          <h5>Zapraszamy !</h5>
                        </div>
                      </div>
                
                    </div>
                  </div>
                </div>
             