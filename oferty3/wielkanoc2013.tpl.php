
                <div class="main section" id="main">
                  <div class="widget Blog" id="Blog1">
                    <div class="blog-posts hfeed">
                      <div class="post hentry uncustomized-post-template">
                        <h2 class="post-title entry-title">Święta Wielkanocne 2013r w Zawoi</h2>
                        <div class="entry">
                          <!--<img border="0" src="media/images/obiekt.jpg" align="left" hspace="10"/>-->
                          <div align="center"> <font color="#660000"></font>
                          
                            <em>Położony u stóp Królowej Beskidów – Babiej Góry (1725m npm)<br>
                              Ośrodek Wypoczynkowy ZAWOJANKA zaprasza na turnus świąteczny</em>
                          </div><br />
                          <h7>Termin I : od 28 marca 2013 r. – czwartek – do 2 kwietnia 2013 r. – wtorek.<br>
                            Termin II: 3 lub 4 dni pobytu – w w/w okresie<br>
                            Turnusy rozpoczynają się obiadem, a kończą śniadaniem. </h7><br /><br />
                          <h4>☺ ŚWIADCZENIA:</h4>
                          <ul>
                            <li>Zakwaterowanie w pokojach 2, 3, 4-os. z łazienkami i tv</li>
                            <li> Wyżywienie: 3 posiłki/dziennie + menu Świąteczne</li>
                            <li> Ognisko z pieczeniem kiełbasek i grzańcem w  poniedziałek Wielkanocny</li>
                            <li> Wieczorek taneczny i karaoke w I-szym dniu świąt</li>
                          </ul><br />
                          <h4>☺ PROPONUJEMY za dodatkową odpłatnością:</h4>
                          <ul>
                            <li>Przejazd wozami (forma kuligu) na poświęcenie pokarmów do pobliskiego kościoła– 15 zł </li>
                            <li> Wycieczkę pieszą na Halę Barankową –  spotkanie z Góralami – ok. 10 zł</li>
                            <li> Wycieczkę pieszą w pasmo Babiej Góry  (1725m npm) – ok. 15 zł</li>
                            <li> Wycieczkę pieszą do Ośrodka Edukacyjnego BPN i Skansenu „Markowe Rówienki” – 8 zł</li>
                            <li> Wycieczki piesze i rowerowe po terenie Zawoi, koszt wypożyczenia roweru 25 zł za szt na cały dzień</li>
                            <li> Zajęcia nauki jazdy konnej – 50 zł godzina, stadnina koni 1 km od ośrodka</li>
                            <li> Quady – 100 zł / godz, Paintaball – 50 zł </li>
                            <li> Wycieczkę autokarową do Orawskiego Parku Etnograficznego - Skansen w Zubrzycy – 17 zł</li>
                            <li> Wycieczkę autokarową do Suchej Beskidzkiej,  mały Wawel, Grota solno-jodowa, basen – 30 zł</li>
                            <li> Wycieczka autokarowa: Oravice Słowacja – baseny termalne – 25 zł (bez biletu wstępu, bilet ok. 50 zł)</li>
                          </ul><br />
                          <h4>☺ KOSZT POBYTU:</h4>
                          <p><strong>350 zł</strong> – pobyt w turnusie z pełnym pakietem <br>
                            <strong>80 zł</strong> - osobodoba przy pobytach 3 dniowych lub <strong>75 zł</strong> - 4 dniowych <br>
                            zniżki dla dzieci – udzielane indywidualnie w zależności od wieku od 10 - 20%<br>
                            Rodziny: odpłatność za 2 osoby dorosłe + 1 dziecko ze zniżką 10%, 2-gie gratis (wiek do 15 lat)</p>
                          <p>
                          <h3 align="center"> </h3>
                          <p align="center"><img src="http://witryny.in-poland.net/pliki/wielkanoc2013.jpg"  alt="" align="middle"/></p>
                          <h5>Zapraszamy !</h5>
                        </div>
                      </div>
                      <!-- google_ad_section_end -->
                    </div>
                  </div>
                </div>
           